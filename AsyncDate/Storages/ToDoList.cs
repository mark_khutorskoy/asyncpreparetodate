﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncDate.Storages
{
    public static class ToDoList
    {
        public const string PrepareToDate = "подготовиться к свиданию";

        public const string FinishWork = "разобраться с рабочими документами";
        public const string PickUpClothes = "забрать костюм из химчистки";
        public const string GetFlowers = "сходить в цветочный магазин и купить букет лилий";
        public const string OrderFood= "заказать еду на вечер";
        public const string GetFood = "получить еду у курьера";
        public const string GetMovie = "скачать фильм на вечер";
    }
}
