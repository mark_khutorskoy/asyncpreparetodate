﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncDate.Storages
{
    public static class IntroInfo
    {
        public const string ReadMe = "ТЗ: Молодой человек хочет устроить идеальное свидание этим вечером, но для этого ему нужно сделать несколько дел." +
                                     "\r\nЧтобы все успеть, ему нужно правильно распределить выполнение дел. " +
                                     "\r\nДанная программа демонстрирует асинхронное выполнение дел с помщью async/await.\r\n" +
                                     "-----------------------------------------------------------------------------------\r\n";
    }
}
