﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsyncDate.Storages;

namespace AsyncDate
{
    public class Program
    {
        /// <summary>
        /// Выводит описание задачи
        /// </summary>
        public static void Description()
        {
            Console.WriteLine(IntroInfo.ReadMe);
        }

        public static void Main(string[] args)
        {
            Description();

            var tasks = new TasksHandler.TasksHandler();
            tasks.StartTasks();

            Console.ReadLine();
        }
    }
}
