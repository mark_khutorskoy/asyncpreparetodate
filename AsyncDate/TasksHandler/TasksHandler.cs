﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AsyncDate.Storages;

namespace AsyncDate.TasksHandler
{
    public class TasksHandler
    {
        /// <summary>
        /// Скачивает фильм в фоновом режиме
        /// </summary>
        public async void GetMovie()
        {
            await Task.Run(() => TasksImpl.GetMovie());
        }


        /// <summary>
        /// Чтобы получить еду нужно сначала ее заказать, а потом дождаться курьера
        /// </summary>
        public async void GetFoodAsync()
        {
            TasksImpl.OrderFood();
            //Console.WriteLine("ДО AWAIT");
            await Task.Run(() => TasksImpl.ReceiveFood()); 
        }

        /// <summary>
        /// Выполняет несовместимые задания асинхронно
        /// </summary>
        public void FinishSyncTasksAsync()
        {
            //await Task.Run(() => TasksImpl.FinishWork());
            //await Task.Run(() => TasksImpl.PickUpClothes());
            //await Task.Run(() => TasksImpl.GetFlowers());
            TasksImpl.FinishWork();
            TasksImpl.PickUpClothes();
            TasksImpl.GetFlowers();
        
        }

        /// <summary>
        /// Выполняет все необходимые задания
        /// </summary>
        public void StartTasks()
        {
            //var getMovie = new Task(TasksImpl.GetMovie);
            //getMovie.Start();
            GetMovie();
            //Thread.Sleep(1000); //для вывода имитирующего человека
            GetFoodAsync();           
            FinishSyncTasksAsync();
        }
    }
}
