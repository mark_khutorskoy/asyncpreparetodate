﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AsyncDate.Storages;

namespace AsyncDate.TasksHandler
{
    public static class TasksImpl
    {
        public static void FinishWork()
        {
            Console.WriteLine("Задача " + ToDoList.FinishWork + " запущена...");
            Thread.Sleep(10000);
            Console.WriteLine("Задача " + ToDoList.FinishWork + " выполнена");
        }

        public static void PickUpClothes()
        {
            Console.WriteLine("Задача " + ToDoList.PickUpClothes + " запущена...");
            Thread.Sleep(10000);
            Console.WriteLine("Задача " + ToDoList.PickUpClothes + " выполнена");
        }

        public static void GetFlowers()
        {
            Console.WriteLine("Задача " + ToDoList.GetFlowers + " запущена...");
            Thread.Sleep(5000);
            Console.WriteLine("Задача " + ToDoList.GetFlowers + " выполнена");
        }

        public static void GetMovie()
        {
            Console.WriteLine("Задача " + ToDoList.GetMovie + " запущена...");
            Thread.Sleep(40000);
            Console.WriteLine("Задача " + ToDoList.GetMovie + " выполнена");
        }

        public static void OrderFood()
        {
            Console.WriteLine("Задача " + ToDoList.OrderFood + " запущена...");
            Thread.Sleep(500);
            Console.WriteLine("Задача " + ToDoList.OrderFood + " выполнена");
        }

        public static void ReceiveFood()
        {
            Console.WriteLine("Задача " + ToDoList.GetFood + " запущена...");
            Thread.Sleep(40000);
            Console.WriteLine("Задача " + ToDoList.GetFood + " выполнена");
        }

    }
}
